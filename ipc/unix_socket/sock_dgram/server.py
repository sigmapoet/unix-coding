#!/usr/bin/python
#coding=utf-8
#client
import socket
import sys
import time
import os

BUF_LEN = 1024
cnt = 0

server_address = './server.sock'
client_address = './client.sock'

try:
    os.unlink(server_address)  # 用于删除一个文件
except OSError:
    if os.path.exists(server_address):
        raise

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
sock.bind(server_address)

try:
    while True:
        data = sock.recvfrom(BUF_LEN)
        print("recv: %s"%data[0].decode())

        message = 'recv msg, ret ack = ' + str(cnt)
        cnt = cnt + 1
        sock.sendto(message.encode(), client_address)

        time.sleep(1)
finally:
    sock.close()
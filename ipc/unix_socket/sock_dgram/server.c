#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUF_LEN 1024

char buffer[BUF_LEN];

int main()
{
    int ret;
    int sockfd;
    struct sockaddr_un server_addr, client_addr;
    struct stat stbuf;
    char* server_sock_file = "server.sock";
    char* client_sock_file = "client.sock";

    if(stat(server_sock_file, &stbuf) != -1) {
        if(S_ISSOCK(stbuf.st_mode))
            unlink(server_sock_file);
    }

    sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(sockfd == -1) {
        perror("socket error");
        return -1;
    }

    bzero(&server_addr, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, server_sock_file);

    bzero(&client_addr, sizeof(client_addr));
    client_addr.sun_family = AF_UNIX;
    strcpy(client_addr.sun_path, client_sock_file);

    ret = bind(sockfd, (struct sockaddr *)(&server_addr), sizeof(struct sockaddr_un));
    if(ret == -1) {
        close(sockfd);
        perror("bind error");
        return ret;
    }

    for(;;) {
        memset(buffer, 0, sizeof(buffer));

        ret = recvfrom(sockfd, buffer, BUF_LEN, 0, NULL, NULL);
        if(ret == -1) {
            perror("recvfrom error");
            close(sockfd);
            return ret;
        }
        printf("recv: %s\n\r", buffer);

        memset(buffer, 0, sizeof(buffer));
        static int cnt = 0;
        sprintf(buffer, "recv msg, ret ack = %d", cnt++);

        ret = sendto(sockfd, buffer, strlen(buffer), 0, (struct sockaddr *)(&client_addr), sizeof(client_addr));
        if(ret == -1) {
            perror("sendto error");
            close(sockfd);
            return ret;
        }

        sleep(1);
    }

    close(sockfd);
    
    return 0;
}
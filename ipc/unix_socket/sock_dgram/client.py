#!/usr/bin/python
#coding=utf-8
#client
import socket
import sys
import time
import os

BUF_LEN = 1024
cnt = 0

server_address = './server.sock'
client_address = './client.sock'

try:
    os.unlink(client_address)  # 用于删除一个文件
except OSError:
    if os.path.exists(client_address):
        raise

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
sock.bind(client_address)

try:
    while True:
        message = 'send msg, id = ' + str(cnt)
        cnt = cnt + 1
        sock.sendto(message.encode(), server_address)

        data = sock.recvfrom(BUF_LEN)
        print("recv: %s"%data[0].decode())

        time.sleep(1)
finally:
    sock.close()

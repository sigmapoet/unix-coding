#!/usr/bin/python
#coding=utf-8
#client
import socket
import sys
import time

BUF_LEN = 1024
cnt = 0
sock = socket.socket(socket.AF_UNIX,socket.SOCK_STREAM)
server_address = './test.sock'  

print(sys.stderr,'connection to %s'%server_address)

try:
    sock.connect(server_address)
except (socket.error,msg):
    print(sys.stderr,msg)
    sys.exit(1)

try:
    while True:
        message = 'send msg, id = ' + str(cnt)
        cnt = cnt + 1
        sock.sendall(message.encode())
        data = sock.recv(BUF_LEN)
        print('recv: %s'%data.decode())
        time.sleep(1)

finally:
    print(sys.stderr,'closing socket')
    sock.close()
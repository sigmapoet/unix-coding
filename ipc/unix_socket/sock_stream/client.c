#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUF_LEN 1024

char buffer[BUF_LEN];

int main()
{
    int ret;
    int sockfd;
    struct sockaddr_un server_addr;
    char *dstAddr = "test.sock";

    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockfd == -1)
        return -1;
    
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, dstAddr);

    ret = connect(sockfd, (struct sockaddr *)(&server_addr), sizeof(struct sockaddr_un));
    if(ret == -1) {
        close(sockfd);
        return -1;
    }

    for(;;) {
        memset(buffer, 0, sizeof(buffer));
        static int cnt = 0;
        sprintf(buffer, "send msg, id = %d", cnt++);

        ret = write(sockfd, buffer, strlen(buffer));
        if(ret == -1) {
            close(sockfd);
            perror("write error");
            return ret;
        }       

        memset(buffer, 0, sizeof(buffer));
        ret = read(sockfd, buffer, BUF_LEN);
        if(ret == -1) {
            close(sockfd);
            perror("read error");
            return ret;
        }
        printf("recv: %s\n\r", buffer);

        sleep(1);
    }

    close(sockfd);

    return 0;
}
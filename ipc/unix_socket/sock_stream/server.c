#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUF_LEN 1024

char buffer[BUF_LEN];

int main()
{
    int ret;
    int sockfd, connfd;
    struct sockaddr_un server_addr, client_addr;
    struct stat stbuf;
    char *srcAddr = "test.sock";

    if(stat(srcAddr, &stbuf) != -1) {
        if(S_ISSOCK(stbuf.st_mode))
            unlink(srcAddr);
    }

    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockfd == -1)
        return -1;

    bzero(&server_addr, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, srcAddr);

    ret = bind(sockfd, (struct sockaddr *)(&server_addr), sizeof(struct sockaddr_un));
    if(ret == -1) {
        close(sockfd);
        return ret;
    }

    ret = listen(sockfd, 4);
    if(ret == -1) {
        close(sockfd);
        return ret;
    }

    int client_len = sizeof(client_addr);
    connfd = accept(sockfd, (struct sockaddr*) &client_addr, &client_len);
    if(connfd < 0) {
        perror("accept error");
        return -1;
    }

    for(;;) {
        memset(buffer, 0, sizeof(buffer));
        
        ret = read(connfd, buffer, BUF_LEN);
        if(ret == -1) {
            close(connfd);
            perror("read error");
            return ret;
        }
        printf("recv: %s\n\r", buffer);

        memset(buffer, 0, sizeof(buffer));
        static int cnt = 0;
        sprintf(buffer, "recv msg, ret ack = %d", cnt++);
        
        ret = write(connfd, buffer, strlen(buffer));
        if(ret == -1) {
            close(connfd);
            perror("write error");
            return ret;
        }

        sleep(1);
    }

    close(sockfd);

    return 0;
}
#!/usr/bin/python
#coding=utf-8
#client
import socket
import sys
import time
import os

BUF_LEN = 1024
cnt = 0

server_address = './test.sock'

try:
    os.unlink(server_address)  # 用于删除一个文件
except OSError:
    if os.path.exists(server_address):
        raise

sock = socket.socket(socket.AF_UNIX,socket.SOCK_STREAM)
sock.bind(server_address)
sock.listen(5)

connection, client_addr = sock.accept()
try:
    while True:
        data = connection.recv(BUF_LEN)
        print('recv: %s'%data.decode())

        message = 'recv msg, ret ack = ' + str(cnt)
        cnt = cnt + 1
        connection.sendall(message.encode())
finally:
    connection.close()
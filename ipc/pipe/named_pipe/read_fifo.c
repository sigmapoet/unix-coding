#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/stat.h>
#include <asm/errno.h>
#include <fcntl.h>

int main()
{
    umask(0);   //保证进程文件拥有最大的默认权限

    int ret;
    int fd;
    char readbuf[50];

    ret = mkfifo("./pipe.fifo", 0644);
    if(ret < 0 && errno != EEXIST) {
        //判断文件已经存在之外的错误
        perror("mkfifo error");
        return -1;
    }

    fd = open("./pipe.fifo", O_RDONLY);
    if(fd < 0) {
        perror("open error");
        return -1;
    }

    while(1) {
        memset(readbuf, 0, sizeof(readbuf));
        ret = read(fd, readbuf, sizeof(readbuf));
        if(ret < 0) {
            perror("read error");
            close(fd);
            return -1;
        }
        printf("read %d bytes, message: %s\n\r", ret, readbuf);
        sleep(1);
    }

    return 0;
}
/*
有名管道，可以用于无亲缘关系的进程间通信。
使用mkfifo函数创建了管道的标识符
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/stat.h>
#include <asm/errno.h>
#include <fcntl.h>

int main()
{
    umask(0);   //保证进程文件拥有最大的默认权限

    int ret;
    int fd;
    char *s = "Named pipe test message.";

    ret = mkfifo("./pipe.fifo", 0644);
    if(ret < 0 && errno != EEXIST) {
        //判断文件已经存在之外的错误
        perror("mkfifo error");
        return -1;
    }

    fd = open("./pipe.fifo", O_WRONLY);
    if(fd < 0) {
        perror("open error");
        return -1;
    }

    while(1) {
        write(fd, s, strlen(s));
        sleep(1);
    }

    return 0;
}
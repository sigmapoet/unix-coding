/*
    无名管道（无标识符，标识符是一个可见于文件系统的管道文件），只能用于有亲缘关系的进程（通常指父子关系），有亲缘关系，才能把管道的文件描述符传递给亲缘进程
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

int main()
{
    pid_t pid;
    int fd[2] = {0};
    int ret;
    char *s = "Pipe test message.";
    char readbuf[50];

    ret = pipe(fd); //fd[0]用于读  fd[1]用于写  半双工   数据单向的
    if(ret == -1) {
        printf("pipe create error\n\r");
        return -1;
    }

    //查看Pipe buffer size大小   一般为4096
    //或者ulimit -a看
    long pipe_buf_size = 0;
    pipe_buf_size = fpathconf(fd[0], _PC_PIPE_BUF);
    printf("pipe buffer size: %ld bytes\n\r", pipe_buf_size);

    pid = fork();
    if(pid < 0) {
        printf("fork create child process failed\n\r");
        return -1;
    }
    else if(pid == 0) {  //child process
        printf("[child process] created.\n\r");
        while(1) {
            memset(readbuf, 0, sizeof(readbuf));
            ret = read(fd[0], readbuf, sizeof(readbuf));    //无数据可读，则阻塞
            printf("[child process] read %d bytes, message: %s\n\r", ret, readbuf);
            sleep(1);
        }
    }
    else {  //parent process
        printf("[parent process] created.\n\r");
        while(1) {
            write(fd[1], s, strlen(s));     //PIPE_BUF满，则阻塞
            sleep(1);
        }
    }
}